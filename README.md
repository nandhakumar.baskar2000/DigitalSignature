<p align="center">
  <img src="https://gitlab.com/nandhakumar.baskar2000/DigitalSignature/-/raw/main/halleyx_sign_1.jpg" width="350" title="hover text">
  Here You can draw your own signature.
</p

<p align="center">
  <img src="https://gitlab.com/nandhakumar.baskar2000/DigitalSignature/-/raw/main/halleeyx_sign_2.jpg" width="350" title="hover text">
  The signaturewhich you drawn will be like this and the next you can able to add in the file.
</p


<p align="center">
  <img src="https://gitlab.com/nandhakumar.baskar2000/DigitalSignature/-/raw/main/halleyx_sign_3.jpg" width="350" title="hover text">
  This is how the signature will be attached in the document.
</p

<p align="center">
  <img src="https://gitlab.com/nandhakumar.baskar2000/DigitalSignature/-/raw/main/halleyx_4.jpg" width="350" title="hover text">
  Here there is download button, you can click that button and download the file and you can viewit there the signature which you made will be there.
</p

#  Digital Signature Web App

## Initial setup

Before you begin, make sure your development environment includes [Node.js](https://nodejs.org/en/).

## Install

```
git clone https://github.com/PDFTron/webviewer-react-sample.git
cd webviewer-react-sample
npm install
```

## Run

```
npm start
```

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `build/` directory. See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

To test the build directory locally you can use [serve](https://www.npmjs.com/package/serve) or [http-server](https://www.npmjs.com/package/http-server). In case of serve, by default it strips the .html extension stripped from paths. We added serve.json configuration to disable cleanUrls option. 

## WebViewer APIs

See [API documentation](https://www.pdftron.com/documentation/web/guides/ui/apis).
